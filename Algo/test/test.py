import unittest
from KMeans import KMeans
import cv2

class TestKMeans(unittest.TestCase):

    def setUp(self):
        path = 'test_chien.jpg'
        self.image = cv2.imread(path)
        self.km = KMeans(n_clusters=50, batch_size=1000, max_iter=100)
        self.new_image = self.km.fit_transform(self.image.reshape(-1, 3)).reshape(self.image.shape)

    def test_shape(self):
        self.assertEqual(self.image.shape, self.new_image.shape)

    def test_n_clusters(self):
        self.assertEqual(len(self.km.cluster_centers_), 50)

    def test_dimension(self):
        d2 = self.km.cluster_centers_
        self.km.fit_transform(self.image.reshape(-1, 1))
        d1 = self.km.cluster_centers_
        self.assertEqual(d1.shape[0], d2.shape[0])

if __name__ == '__main__':
    unittest.main()