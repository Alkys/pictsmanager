from flask import Flask
from flask import request
import requests
from flask import send_file
from image_compression import ImageCompression 
import numpy as np
from KMeans import KMeans

app = Flask(__name__)

@app.route("/compile", methods=['POST'])
def compression():

    """ Used to compress image. Input as png and output as json"""

    file = request.files['image']
    image = np.array(Image.open(file.stream))

    fn = ImageCompression(image)
    
    height = image.shape[0]
    width = image.shape[1]

    factor = height / width

    if factor > 1 and height > 500:
        fn.get_image_resized(int(500 / factor), 500)
    elif factor < 1 and width > 500:
        fn.get_image_resized(500, int(500 * factor))
    
    fn.get_image_clustered(KMeans)

    return {'image': fn.image_clustered.tolist()}
    