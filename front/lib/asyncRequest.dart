import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//TO DEGUG
var URL = "http://localhost:8080";

class AsyncRequest extends StatefulWidget {
  @override
  _AsyncRequestState createState() => _AsyncRequestState();
}

class _AsyncRequestState extends State<AsyncRequest> {

  Future<void> makeGetRequest() async {
    final url = Uri.parse('$URL/posts');
    http.Response response = await http.get(url);
    print('Status code: ${response.statusCode}');
    print('Headers: ${response.headers}');
    print('Body: ${response.body}');
  }

  Future<void> makePostRequest(String filename) async {
    //Use local url to post on flask app (that compress and send the image to back)
    var request = http.MultipartRequest('POST', Uri.parse('http://127.0.0.1:5000/compile'));
    request.files.add(
      await http.MultipartFile.fromPath(
        'picture',
        filename
      )
    );
    var response = await request.send();
    print('Status code: ${response.statusCode}');
  }

  Future<void> makePutRequest() async {
    final url = Uri.parse('$URL/posts/1');
    final headers = {"Content-type": "application/json"};
    final json = '{"title": "Hello", "body": "body text", "userId": 1}';
    final response = await http.put(url, headers: headers, body: json);
    print('Status code: ${response.statusCode}');
    print('Body: ${response.body}');
  }

  Future<void> makeDeleteRequest() async {
    final url = Uri.parse('$URL/posts/1');
    final response = await http.delete(url);
    print('Status code: ${response.statusCode}');
    print('Body: ${response.body}');
  }

  @override
  Widget build(BuildContext context) {

  }
}
